window.onload = function() {
    loadMovies();
};

function loadMovies() {
    let frag = document.createDocumentFragment();

    for (let key in all_titles) {
        if (all_titles.hasOwnProperty(key)) {
            let option = document.createElement("OPTION");
            option.textContent = key;
            option.value = key;
            frag.appendChild(option);
        }
    }
    let movies = document.querySelector('#movies');
    movies.appendChild(frag);
}
async function getWord() {
    movie = document.querySelector('#movie').value;
    occurances = document.querySelector('#occurances').value;
    if (!all_titles[movie] || occurances < 0) {
        alert("Invalid Input. Do not modify auto complete movie title. Keep number of hydration above 0");
    }
    // Patch to clean up name
    filename = all_titles[movie]
    filename = filename.split(".json")[0];
    filename = filename.replace(/[.,\/#!$%\^&\*;:{}=\_`~()]/g,"");
    let response = await fetch('occurances/'+filename+".json");
    if (response.ok) {
        let json = await response.json();
        let words = Object.entries(json);
        filtered = words.filter(([key, value]) => value == occurances);
        mapped = filtered.map(([key, value]) => key);
        let result = document.querySelector('#result');
        if (mapped.length < 1) {
            maxIter = words.length;
            while (mapped.length < 1 && maxIter) {
                occurances--;
                filtered = words.filter(([key, value]) => value == occurances);
                mapped = filtered.map(([key, value]) => key);
                maxIter--;
            }
            if (!maxIter) {
                alert("Could not find word");
                result.innerHTML = "";
                return
            } else {
                alert(`Could not find word which exactly matches number of hydrations. Using ${occurances} hydrations instead.`);
            }
        }
        document.querySelector('#occurances').value = occurances;
        word = mapped[Math.floor((Math.random() * mapped.length) + 0)];
        result.innerHTML = `Hydrate everytime you hear the word <i>${word}</i>`;
    }
}

